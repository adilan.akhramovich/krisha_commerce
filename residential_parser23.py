import os
import time
import json
import random
import requests
import psycopg2
import schedule
from bs4 import BeautifulSoup
from datetime import datetime, timedelta

from shapely.wkb import dumps
from shapely.geometry import Point

# Параметры подключения к базе данных PostgreSQL
db_params = {
    'dbname': 'rwh_datalake',
    'user': 'rwh_analytics',
    'password': '4HPzQt2HyU@',
    'host': '172.30.227.205',
    'port': '5439'
}

try:
    with open('fast_urls.txt', 'r') as file:
        fast_urls = [line.strip() for line in file.readlines()]
except:
    
    fast_urls = []
    
proxy_index = 0
    
# прокси сервера на выбор
proxies = [
    {
       'socks5' : '45.77.56.114:30205',
    },
    {
        'socks5' : '46.4.96.137:1080',
    },
    {
        'socks5' : '178.62.193.19:1080',
    },
    {
        'socks5' : '185.153.198.226:32498',
    },
    {
        'socks5' : '164.68.98.169:9300',
    },
    {
        'https': '81.171.24.199:3128',
    },
    {
        'https': '173.192.21.89:80',
    },
    {
        'https': '164.68.105.235:3128',
    },
    {
        'https': '212.112.97.27:3128',
    },
    {
        'https': '84.201.254.47:3128',
    },
    {
        'https': '83.77.118.53:17171',
    },
    {
        'http': '95.156.82.35:3128',
    },
    {
        'http' : '20.163.133.5:80',
    },
    {
        'http' : '154.65.39.8:80',
    },
    {
        'http' : '103.163.227.245:3125',
    },
    {
        'http' : '144.48.81.145:8080',
    },
    {
        'http' : '52.230.98.217:3128',
    }
]

user_agents = [
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/16.1 Safari/605.1.15',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 13_1) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/16.1 Safari/605.1.15',
    'Mozilla/5.0 (iPhone; CPU iPhone OS 17_0 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/117.0.5938.117 Mobile/15E148 Safari/604.1',
    'Mozilla/5.0 (iPad; CPU OS 17_0 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/117.0.5938.117 Mobile/15E148 Safari/604.1',
    'Mozilla/5.0 (iPod; CPU iPhone OS 17_0 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/117.0.5938.117 Mobile/15E148 Safari/604.1',
    'Mozilla/5.0 (Linux; Android 10) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.5938.153 Mobile Safari/537.36',
    'Mozilla/5.0 (Linux; Android 10; SM-A205U) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.5938.153 Mobile Safari/537.36',
    'Mozilla/5.0 (Linux; Android 10; SM-A102U) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.5938.153 Mobile Safari/537.36',
    'Mozilla/5.0 (Linux; Android 10; SM-G960U) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.5938.153 Mobile Safari/537.36',
    'Mozilla/5.0 (Linux; Android 10; SM-N960U) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.5938.153 Mobile Safari/537.36',
    'Mozilla/5.0 (Linux; Android 10; LM-Q720) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.5938.153 Mobile Safari/537.36',
    'Mozilla/5.0 (Linux; Android 10; LM-X420) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.5938.153 Mobile Safari/537.36',
    'Mozilla/5.0 (Linux; Android 10; LM-Q710(FGN)) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.5938.153 Mobile Safari/537.36'
]

# Function to run the crime data insertion
def run_parse():
    
    proxy = proxies[proxy_index]

    start = datetime.now()
    pages = 0
    url = 'https://krisha.kz/arenda/kvartiry/almaty/?page='

    # случайно выбрать user-agent из предложенных
    headers = {'User-Agent': random.choice(user_agents)}

    # сделать первый пробный запрос чтобы узнать сколько страниц насчитывает список объявлений.
    req = requests.get(url + str(1), headers=headers, proxies=proxy)
    src = req.text
    soup = BeautifulSoup(src, "lxml")

    # достать максимальное количество страниц в списке объявлений из пагинатора
    pages = max([int(item.text) if any(char.isdigit() for char in item.text) else 0 for item in soup.find_all('a', class_='paginator__btn')])

    print('all pages:', pages)

    for page in reversed(range(1, pages + 1)):

        #-------------------------------------------------------------------------------------------------
        # пропустить если встретился дубликат

        if (('page:' + str(page)) in fast_urls):
            continue


        # Задержка между запросами
        time.sleep(31) # 31

        #-------------------------------------------------------------------------------------------------
        # Получить запрос на страницы списка объявлений

        req = requests.get(url + str(page), headers=headers, proxies=proxy)
        src = req.text
        soup = BeautifulSoup(src, "lxml")


        #-------------------------------------------------------------------------------------------------
        # Если на странице нет объявлени то пропустить эту страницу (на всякий случай)

        size = len(soup.find_all('div', class_='a-card'))

        if size == 0:

            print('size=0    page:', page)
            continue

        #-------------------------------------------------------------------------------------------------
        # Получить данные по объявлениям

        for index, item in enumerate(soup.find_all('div', class_='a-card')):
            
            try:

                ID = item.get('data-id')
                uuid = item.get('data-uuid')

                title = item.find('a', class_='a-card__title').text

                item_url = 'https://krisha.kz' + item.find('a', class_='a-card__title').get('href')

                price = str(item.find('div', class_='a-card__price'))
                price = price.replace('<div class="a-card__price">', '')
                price = price.replace('</div>', '')
                price = price.replace('\xa0', '')
                price = price.strip()

                #-------------------------------------------------------------------------------------------------
                # Check for duplicates in database

                # Establish a connection to the database
                conn = psycopg2.connect(**db_params)

                # Create a cursor
                cursor = conn.cursor()

                # Perform a SELECT query
                query = "SELECT * FROM krisha_residential WHERE url = %s AND parse_datetime > %s"
                ten_days_ago = datetime.now() - timedelta(days=10)

                cursor.execute(query, (item_url, ten_days_ago))
                result = cursor.fetchall()

                # Close the cursor and the database connection
                cursor.close()
                conn.close()

                if result:
                    continue

                #-------------------------------------------------------------------------------------------------
                # задержка перед каждым запуском чтобы ip адрес прокси сервера не попал в блок

                time.sleep(55) # 55

                #-------------------------------------------------------------------------------------------------
                # запрос на следующее объявление

                item_req = requests.get(item_url, headers=headers, proxies=proxy)
                item_src = item_req.text
                item_soup = BeautifulSoup(item_src, "lxml")

                #-------------------------------------------------------------------------------------------------
                # получение данных с "offer__short-description"

                title = None
                room = None
                payment_period = None
                address = None
                floor = None
                area = None
                renovation = None
                is_furnitured = None
                security = None
                priv_dorm = None
                complex_name = None

                if "Страница не найдена" in str(item_soup):
                    continue

                if 'service-info__title' in str(item_soup):

                    title = item_soup.find('h1', class_='service-info__title').text.strip().split(' • ')

                else:

                    title = item_soup.find('div', class_='offer__advert-title').find('h1').text.strip().split(', ')

                room = title[0]
                if len(title) == 2:
                    payment_period = title[1]
                else:
                    payment_period = title[2]

                if 'offer__info-item' in str(item_soup):

                    try:
                        for parameter in item_soup.find_all('div', class_='offer__info-item'):

                            if parameter.find('div', class_='offer__location').find('span') != None:

                                address = parameter.find('div', class_='offer__location').find('span').text.strip() + ', ' + title[3]

                            elif parameter.get('data-name') == 'live.square':

                                area = parameter.find('div', class_='offer__advert-short-info').text.strip()

                            elif parameter.get('data-name') == 'house.year':

                                year = int(parameter.find('div', class_='offer__advert-short-info').text.strip())

                            elif parameter.get('data-name') == 'com.location':

                                location = parameter.find('div', class_='offer__advert-short-info').text.strip()

                            elif parameter.get('data-name') == 'map.complex':

                                complex_name = parameter.find('div', class_='offer__advert-short-info').text.strip()

                            elif parameter.get('data-name') == 'flat.floor':

                                floor = parameter.find('div', class_='offer__advert-short-info').text.strip()

                            elif parameter.get('data-name') == 'kitchen_studio':                

                                kitchen_studio = parameter.find('div', class_='offer__advert-short-info').text.strip()

                            elif parameter.get('data-name') == 'flat.rent_renovation':

                                renovation = parameter.find('div', class_='offer__advert-short-info').text.strip()

                            elif parameter.get('data-name') == 'live.furniture':

                                furniture_rate = parameter.find('div', class_='offer__advert-short-info').text.strip()

                            elif parameter.get('data-name') == 'flat.priv_dorm':

                                priv_dorm = parameter.find('div', class_='offer__advert-short-info').text.strip()

                            elif parameter.get('data-name') == 'flat.security':

                                security = parameter.find('div', class_='offer__advert-short-info').text.strip()

                    except:
                        pass

                    #-------------------------------------------------------------------------------------------------
                    # получение данных с "offer__parameters"

                    furniture = None
                    facilities = None
                    separated_toilet = None
                    toilet_count = None
                    bathroom = None
                    window_side = None
                    balcony_count = None
                    loggia_count = None
                    who_match = None

                    try:
                        offer__parameters = item_soup.find('div', class_='offer__parameters')

                        for parameter in offer__parameters.find_all('dl'):

                            if parameter.find('dt').get('data-name') == 'flat.furniture':

                                furniture = parameter.find('dd').text.strip()

                            elif parameter.find('dt').get('data-name') == 'flat.facilities':

                                facilities = parameter.find('dd').text.strip()

                            elif parameter.find('dt').get('data-name') == 'separated_toilet':

                                separated_toilet = parameter.find('dd').text.strip()

                            elif parameter.find('dt').get('data-name') == 'toilet_count':

                                toilet_count = parameter.find('dd').text.strip()

                            elif parameter.find('dt').get('data-name') == 'bathroom':

                                bathroom = parameter.find('dd').text.strip()

                            elif parameter.find('dt').get('data-name') == 'window_side':

                                window_side = parameter.find('dd').text.strip()

                            elif parameter.find('dt').get('data-name') == 'balcony_count':

                                balcony_count = parameter.find('dd').text.strip()

                            elif parameter.find('dt').get('data-name') == 'loggia_count':

                                loggia_count = parameter.find('dd').text.strip()

                            elif parameter.find('dt').get('data-name') == 'who_match':

                                who_match = parameter.find('dd').text.strip()

                            elif parameter.find('dt').get('data-name') == 'flat.security':

                                security = parameter.find('dd').text.strip()
                    except:
                        pass

                    #-------------------------------------------------------------------------------------------------
                    # получение данных с "a-text a-text-white-spaces" (Полное текстовое описание объявления)

                    try:
                        description = item_soup.find('div', class_='a-text a-text-white-spaces').text
                    except:
                        description = None  

                else:
                    for parameter in item_soup.find_all('div', class_='attributes__item'):

                        for par_item in parameter.find_all('span', class_='attributes__item-value'):

                            if par_item.get('data-parameter-value') == 'live.square':

                                area = par_item.text.strip()

                            elif par_item.get('data-parameter-value') == 'house.year':

                                year = int(par_item.text.strip())

                            elif par_item.get('data-parameter-value') == 'com.location':

                                location = par_item.text.strip()

                            elif par_item.get('data-parameter-value') == 'map.complex':

                                complex_name = par_item.text.strip()

                            elif par_item.get('data-parameter-value') == 'flat.floor':

                                floor = par_item.text.strip()

                            elif par_item.get('data-parameter-value') == 'kitchen_studio':                

                                kitchen_studio = par_item.text.strip()

                            elif par_item.get('data-parameter-value') == 'flat.rent_renovation':

                                renovation = par_item.text.strip()

                            elif par_item.get('data-parameter-value') == 'live.furniture':

                                furniture_rate = par_item.text.strip()

                            elif par_item.get('data-parameter-value') == 'flat.priv_dorm':

                                priv_dorm = par_item.text.strip()

                            elif par_item.get('data-parameter-value') == 'flat.security':

                                security = par_item.text.strip()

                            elif par_item.get('data-parameter-value') == 'flat.furniture':

                                furniture = par_item.text.strip()

                            elif par_item.get('data-parameter-value') == 'flat.facilities':

                                facilities = par_item.text.strip()

                            elif par_item.get('data-parameter-value') == 'separated_toilet':

                                separated_toilet = par_item.text.strip()

                            elif par_item.get('data-parameter-value') == 'toilet_count':

                                toilet_count = par_item.text.strip()

                            elif par_item.get('data-parameter-value') == 'bathroom':

                                bathroom = par_item.text.strip()

                            elif par_item.get('data-parameter-value') == 'window_side':

                                window_side = par_item.text.strip()

                            elif par_item.get('data-parameter-value') == 'balcony_count':

                                balcony_count = par_item.text.strip()

                            elif par_item.get('data-parameter-value') == 'loggia_count':

                                loggia_count = par_item.text.strip()

                            elif par_item.get('data-parameter-value') == 'who_match':

                                who_match = par_item.text.strip()

                    #-------------------------------------------------------------------------------------------------
                    # получение данных с "a-text a-text-white-spaces" (Полное текстовое описание объявления)

                    try:
                        description = item_soup.find('div', class_='description-block__text').text
                    except:
                        description = None  

                    description-block

                try:
                    jsdata = item_soup.find('script', id='jsdata').text
                except:
                    jsdata = None

                if jsdata != None:

                    jsdata = jsdata.replace('var data =', '').strip().replace('\\\\', '')[:-1]

                parse_datetime = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

                data = json.loads(jsdata)

                for par in title:
                    if 'этаж' in par:
                        floor = par.replace('помесячно', '').strip()


                if 'advert' in data.keys():   

                    if data['advert']['price'] != None:
                        price = data['advert']['price']

                    if data['advert']['square'] != None:
                        area = data['advert']['square']

                    if data['advert']['title'] != None:
                        title = data['advert']['title']

                    if data['advert']['rooms'] != None:
                        room = data['advert']['rooms']

                    if data['advert']['ownerName'] != None:
                        ownerName = data['advert']['ownerName']

                    if data['advert']['userType'] != None:
                        ownerType = data['advert']['userType']

                if 'adverts' in data.keys():   

                    if data['adverts'][0]['uuid'] != None:
                        uuid = data['adverts'][0]['uuid']

                    if data['adverts'][0]['createdAt'] != None:
                        created_date = data['adverts'][0]['createdAt']

                    if data['adverts'][0]['fullAddress'] != None:
                        address = data['adverts'][0]['fullAddress']

                    if data['adverts'][0]['description'] != None:
                        description = data['adverts'][0]['description']

                    if 'map' in list(data['advert'].keys()):
                        if data['advert']['map']['lon'] != None and data['advert']['map']['lat'] != None:
                            geometry = dumps(Point([data['advert']['map']['lon'], data['advert']['map']['lat']])).hex()
                        if data['advert']['map']['lon'] != None:
                            longitude = data['advert']['map']['lon']
                        if data['advert']['map']['lat'] != None:
                            latitude = data['advert']['map']['lat']
                        
            except:
                proxy_index += 1
                if proxy_index >= len(proxies):
                    proxy_index = 0
                proxy = proxies[proxy_index]
                print('proxy switched to:', proxy)

            #-------------------------------------------------------------------------------------------------
            # записать данные файл после каждого объявления, чтобы исключить потерю данных

            row = [
                ID,
                uuid, 
                item_url,
                price,
                title,
                room,
                address,
                floor,
                area,
                renovation,
                is_furnitured,
                security,
                priv_dorm,
                complex_name,
                furniture,
                facilities,
                separated_toilet,
                toilet_count,
                bathroom,
                window_side,
                balcony_count,
                loggia_count,
                who_match,
                ownerName,
                ownerType,
                description,
                jsdata,
                latitude,
                longitude,
                geometry,
                created_date,
                parse_datetime
            ]

            query = '''INSERT INTO krisha_residential (krisha_ID, krisha_UUID, url, price, title, room, address, floor, area, renovation, is_furnitured, security, priv_dorm, complex_name, furniture, facilities, separated_toilet, toilet_count, bathroom, window_side,  balcony_count, loggia_count, who_match, ownerName,  ownerType, description, jsdata, latitude,longitude,geom,created_date, parse_datetime) 
                VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, TO_DATE(%s, 'YYYY-MM-DD'), TO_TIMESTAMP(%s, 'YYYY-MM-DD HH24:MI:SS'))'''

            # Подключение к базе данных PostgreSQL
            conn = psycopg2.connect(**db_params)
            cur = conn.cursor()

            cur.execute(query, row)

            # Фиксация изменений и закрытие соединения
            conn.commit()
            cur.close()
            conn.close()

        #-------------------------------------------------------------------------------------------------
        # записать номер страницы объявлении чтобы пропускать их при повторном запуске

        fast_urls.append(('page:' + str(page)))
        with open("fast_urls.txt", "w", encoding='utf-8') as urls_file:
            for url_item in fast_urls:
                urls_file.write(url_item + "\n")

        #-------------------------------------------------------------------------------------------------
        # отображение индикатора прогресса

        print('   page:', page)

    print("start time:", start.strftime("%Y-%m-%d %H:%M:%S"))
    print("  end time:", datetime.now().strftime("%Y-%m-%d %H:%M:%S"))

    delta = (datetime.now() - start)

    days = delta.days
    hours = (delta.seconds // 3600)
    minutes = (delta.seconds % 3600) // 60
    seconds = delta.seconds % 60

    print("spent time:", days, 'days', str(hours) + ':' + str(minutes) + ':' + str(seconds))

    os.remove("fast_urls.txt")

# Function to calculate the date range for the next 14 days
def calculate_date_range():
    today = datetime.now()
    start_date = today + timedelta(days=0)  # Start from tomorrow
    end_date = today + timedelta(days=25)   # End after 14 days
    return start_date.strftime('%Y-%m-%d 00:00:00'), end_date.strftime('%Y-%m-%d 00:00:00')

# Function to schedule the parse
def schedule_parse():
    start_date, end_date = calculate_date_range()
    print(f"Next parse will be done between {start_date} and {end_date}")
    
    schedule.every(25).days.do(run_parse)

    while True:
        time_left = schedule.idle_seconds()
        if time_left is not None:
            remaining_time = timedelta(seconds=time_left)
            print(f"Time left until next parse: {remaining_time}")
            time.sleep(21600)  # Wait for a minute before checking again
        schedule.run_pending()


if __name__ == "__main__":
    schedule_parse()