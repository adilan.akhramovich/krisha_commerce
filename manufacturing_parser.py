import os
import time
import json
import random
import requests
import psycopg2
import schedule
from bs4 import BeautifulSoup
from datetime import datetime, timedelta

from shapely.wkb import dumps
from shapely.geometry import Point

# Параметры подключения к базе данных PostgreSQL
db_params = {
    'dbname': 'rwh_datalake',
    'user': 'rwh_analytics',
    'password': '4HPzQt2HyU@',
    'host': '172.30.227.205',
    'port': '5439'
}

try:
    with open('fast_urls.txt', 'r') as file:
        fast_urls = [line.strip() for line in file.readlines()]
except:
    
    fast_urls = []
    
proxy_index = 0
    
# прокси сервера на выбор
proxies = [
    {
        'socks5' : '82.196.11.105:1080',
    },
    {
        'socks5' : '47.91.88.100:1080',
    },
    {
        'socks5' : '188.226.141.127:1080',
    },
    {
        'socks5' : '5.189.224.84:10000',
    },
    {
        'socks5' : '103.174.178.133:1020',
    },
    {
        'https': '176.31.200.104:3128',
    },
    {
        'https': '163.172.182.164:3128',
    },
    {
        'https': '5.199.171.227:3128',
    },
    {
        'https': '51.68.207.81:80',
    },
    {
        'https': '217.113.122.142:3128',
    },
    {
        'https': '83.79.50.233:64527',
    },
    {
        'http': '185.118.141.254:808',
    },
    {
        'http' : '115.248.66.131:3129',
    },
    {
        'http' : '103.179.252.86:8181',
    },
    {
        'http' : '200.19.177.120:80',
    },
    {
        'http' : '38.65.174.129:999',
    },
    {
        'http' : '43.153.41.35:3128',
    }
]

user_agents = [
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/16.1 Safari/605.1.15',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 13_1) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/16.1 Safari/605.1.15',
    'Mozilla/5.0 (iPhone; CPU iPhone OS 17_0 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/117.0.5938.117 Mobile/15E148 Safari/604.1',
    'Mozilla/5.0 (iPad; CPU OS 17_0 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/117.0.5938.117 Mobile/15E148 Safari/604.1',
    'Mozilla/5.0 (iPod; CPU iPhone OS 17_0 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/117.0.5938.117 Mobile/15E148 Safari/604.1',
    'Mozilla/5.0 (Linux; Android 10) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.5938.153 Mobile Safari/537.36',
    'Mozilla/5.0 (Linux; Android 10; SM-A205U) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.5938.153 Mobile Safari/537.36',
    'Mozilla/5.0 (Linux; Android 10; SM-A102U) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.5938.153 Mobile Safari/537.36',
    'Mozilla/5.0 (Linux; Android 10; SM-G960U) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.5938.153 Mobile Safari/537.36',
    'Mozilla/5.0 (Linux; Android 10; SM-N960U) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.5938.153 Mobile Safari/537.36',
    'Mozilla/5.0 (Linux; Android 10; LM-Q720) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.5938.153 Mobile Safari/537.36',
    'Mozilla/5.0 (Linux; Android 10; LM-X420) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.5938.153 Mobile Safari/537.36',
    'Mozilla/5.0 (Linux; Android 10; LM-Q710(FGN)) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.5938.153 Mobile Safari/537.36'
]

# Function to run the crime data insertion
def run_parse():
    
    proxy = proxies[proxy_index]
    
    start = datetime.now()
    pages = 0
    url = 'https://krisha.kz/arenda/prombazy/almaty/?page='

    # случайно выбрать user-agent из предложенных
    headers = {'User-Agent': random.choice(user_agents)}

    # сделать первый пробный запрос чтобы узнать сколько страниц насчитывает список объявлений.
    req = requests.get(url + str(1), headers=headers, proxies=proxy)
    src = req.text
    soup = BeautifulSoup(src, "lxml")

    # достать максимальное количество страниц в списке объявлений из пагинатора
    pages = max([int(item.text) if any(char.isdigit() for char in item.text) else 0 for item in soup.find_all('a', class_='paginator__btn')])

    print('all pages:', pages)

    for page in reversed(range(1, pages + 1)):

        #-------------------------------------------------------------------------------------------------
        # пропустить если встретился дубликат

        if (('page:' + str(page)) in fast_urls):
            continue


        # Задержка между запросами
        time.sleep(31) # 31

        #-------------------------------------------------------------------------------------------------
        # Получить запрос на страницы списка объявлений

        req = requests.get(url + str(page), headers=headers, proxies=proxy)
        src = req.text
        soup = BeautifulSoup(src, "lxml")


        #-------------------------------------------------------------------------------------------------
        # Если на странице нет объявлени то пропустить эту страницу (на всякий случай)

        size = len(soup.find_all('div', class_='a-card'))

        if size == 0:

            print('size=0    page:', page)
            continue

        #-------------------------------------------------------------------------------------------------
        # Получить данные по объявлениям

        for index, item in enumerate(soup.find_all('div', class_='a-card')):
            
            try:

                ID = item.get('data-id')

                uuid = item.get('data-uuid')

                title = item.find('a', class_='a-card__title').text

                item_url = 'https://krisha.kz' + item.find('a', class_='a-card__title').get('href')

                price = str(item.find('div', class_='a-card__price'))
                price = price.replace('<div class="a-card__price">', '')
                price = price.replace('</div>', '')
                price = price.replace('\xa0', '')
                price = price.strip()

                address = item.find('div', class_='a-card__subtitle').text
                address = address.replace('\n', '')
                address = address.strip()


                # Задержка между запросами
                time.sleep(55) # 55

                #-------------------------------------------------------------------------------------------------
                # запрос на следующее объявление

                item_req = requests.get(item_url, headers=headers, proxies=proxy)
                item_src = item_req.text
                item_soup = BeautifulSoup(item_src, "lxml")

                #-------------------------------------------------------------------------------------------------
                # получение данных с "offer__short-description"

                object_type = None
                address = None
                year = None
                territory_area = None
                production_area = None
                stock_area = None
                office_area = None
                railway_dead_end = None
                self_station = None
                communications = None
                max_electricity = None

                if "Страница не найдена" in str(item_soup):
                    continue

                if 'service-info__title' in str(item_soup):

                    title = item_soup.find('h1', class_='service-info__title').text.strip().split(' • ')

                else:

                    title = item_soup.find('div', class_='offer__advert-title').find('h1').text.strip().split(', ')

                object_type = title[0]

                try:
                    for parameter in item_soup.find_all('div', class_='offer__info-item'):

                        if parameter.get('data-name') == 'land.square':

                            territory_area = parameter.find('div', class_='offer__advert-short-info').text.strip()

                        elif parameter.get('data-name') == 'indust.prod_square':

                            production_area = parameter.find('div', class_='offer__advert-short-info').text.strip()

                        elif parameter.get('data-name') == 'indust.store_square':

                            stock_area = parameter.find('div', class_='offer__advert-short-info').text.strip()

                        elif parameter.get('data-name') == 'indust.office_square':

                            office_area = parameter.find('div', class_='offer__advert-short-info').text.strip()

                        elif parameter.get('data-name') == 'house.year':

                            year = parameter.find('div', class_='offer__advert-short-info').text.strip()

                        elif parameter.get('data-name') == 'indust.rail':

                            railway_dead_end = parameter.find('div', class_='offer__advert-short-info').text.strip()

                        elif parameter.get('data-name') == 'indust.electr_station':

                            self_station = parameter.find('div', class_='offer__advert-short-info').text.strip()

                        elif parameter.get('data-name') == 'com.communications':

                            communications = parameter.find('div', class_='offer__advert-short-info').text.strip()

                        elif parameter.get('data-name') == 'indust.max_electr':

                            max_electricity = parameter.find('div', class_='offer__advert-short-info').text.strip()

                except:
                    pass  

                #-------------------------------------------------------------------------------------------------
                # получение данных с "offer__parameters"

                try:
                    offer__parameters = item_soup.find('div', class_='offer__parameters')

                    for parameter in offer__parameters.find_all('dl'):

                        if parameter.find('dt').get('data-name') == 'com.communications':

                            communications = parameter.find('dd').text.strip()

                        elif parameter.find('dt').get('data-name') == 'indust.rail':

                            railway_dead_end = parameter.find('dd').text.strip()

                        elif parameter.find('dt').get('data-name') == 'indust.electr_station':

                            self_station = parameter.find('dd').text.strip()

                        elif parameter.find('dt').get('data-name') == 'indust.max_electr':

                            max_electricity = parameter.find('dd').text.strip()

                        elif parameter.find('dt').get('data-name') == 'land.square':

                            territory_area = parameter.find('dd').text.strip()

                        elif parameter.find('dt').get('data-name') == 'indust.prod_square':

                            production_area = parameter.find('dd').text.strip()

                        elif parameter.find('dt').get('data-name') == 'indust.store_square':

                            stock_area = parameter.find('dd').text.strip()

                        elif parameter.find('dt').get('data-name') == 'indust.office_square':

                            office_area = parameter.find('dd').text.strip()

                        elif parameter.find('dt').get('data-name') == 'house.year':

                            year = parameter.find('dd').text.strip()
                except:
                    pass

                #-------------------------------------------------------------------------------------------------
                # получение данных с "a-text a-text-white-spaces" (Полное текстовое описание объявления)

                try:
                    description = item_soup.find('div', class_='a-text a-text-white-spaces').text
                except:
                    description = None

                try:
                    jsdata = item_soup.find('script', id='jsdata').text
                except:
                    jsdata = None

                if jsdata != None:

                    jsdata = jsdata.replace('var data =', '').strip().replace('\\\\', '')[:-1]

                parse_datetime = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

                data = json.loads(jsdata)

                if 'advert' in data.keys():   

                    if data['advert']['price'] != None:
                        price = data['advert']['price']

                    if data['advert']['square'] != None:
                        area = data['advert']['square']

                    if data['advert']['title'] != None:
                        title = data['advert']['title']

                    if data['advert']['rooms'] != None:
                        room = data['advert']['rooms']

                    if data['advert']['ownerName'] != None:
                        ownerName = data['advert']['ownerName']

                    if data['advert']['userType'] != None:
                        ownerType = data['advert']['userType']

                if 'adverts' in data.keys():   

                    if data['adverts'][0]['uuid'] != None:
                        uuid = data['adverts'][0]['uuid']

                    if data['adverts'][0]['createdAt'] != None:
                        created_date = data['adverts'][0]['createdAt']

                    if data['adverts'][0]['fullAddress'] != None:
                        address = data['adverts'][0]['fullAddress']

                    if data['adverts'][0]['description'] != None:
                        description = data['adverts'][0]['description']

                    if 'map' in list(data['advert'].keys()):
                        if data['advert']['map']['lon'] != None and data['advert']['map']['lat'] != None:
                            geometry = dumps(Point([data['advert']['map']['lon'], data['advert']['map']['lat']])).hex()
                        if data['advert']['map']['lon'] != None:
                            longitude = data['advert']['map']['lon']
                        if data['advert']['map']['lat'] != None:
                            latitude = data['advert']['map']['lat']

                geocoding_geometry = None
                        
            except:
                proxy_index += 1
                if proxy_index >= len(proxies):
                    proxy_index = 0
                proxy = proxies[proxy_index]
                print('proxy switched to:', proxy)

            #-------------------------------------------------------------------------------------------------
            # записать данные файл после каждого объявления, чтобы исключить потерю данных

            row = [
                ID,
                uuid, 
                item_url,
                price,
                title,
                object_type,
                address,
                territory_area,
                production_area,
                stock_area,
                office_area,
                year,
                railway_dead_end,
                self_station,
                communications,
                max_electricity,
                ownerName,
                ownerType,
                description,
                jsdata,
                latitude,
                longitude,
                geometry,
                geocoding_geometry,
                created_date,
                parse_datetime
            ]

            query = '''INSERT INTO krisha_manufacturing (
                    krisha_ID, 
                    krisha_UUID, 
                    url, 
                    price, 
                    title, 
                    object_type, 
                    address,
                    territory_area, 
                    production_area, 
                    stock_area, 
                    office_area, 
                    year, 
                    railway_dead_end, 
                    self_station, 
                    communications, 
                    max_electricity, 
                    ownerName,  
                    ownerType, 
                    description, 
                    jsdata, 
                    latitude,
                    longitude,
                    geom,
                    geocoding_geometry,
                    created_date, 
                    parse_datetime) 
                VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, TO_DATE(%s, 'YYYY-MM-DD'), TO_TIMESTAMP(%s, 'YYYY-MM-DD HH24:MI:SS'))'''        

            # Подключение к базе данных PostgreSQL
            conn = psycopg2.connect(**db_params)
            cur = conn.cursor()

            cur.execute(query, row)

            # Фиксация изменений и закрытие соединения
            conn.commit()
            cur.close()
            conn.close()

        #-------------------------------------------------------------------------------------------------
        # записать номер страницы объявлении чтобы пропускать их при повторном запуске

        fast_urls.append(('page:' + str(page)))
        with open("fast_urls.txt", "w", encoding='utf-8') as urls_file:
            for url_item in fast_urls:
                urls_file.write(url_item + "\n")

        #-------------------------------------------------------------------------------------------------
        # отображение индикатора прогресса

        print('   page:', page)

    print("start time:", start.strftime("%Y-%m-%d %H:%M:%S"))
    print("  end time:", datetime.now().strftime("%Y-%m-%d %H:%M:%S"))

    delta = (datetime.now() - start)

    days = delta.days
    hours = (delta.seconds // 3600)
    minutes = (delta.seconds % 3600) // 60
    seconds = delta.seconds % 60

    print("spent time:", days, 'days', str(hours) + ':' + str(minutes) + ':' + str(seconds))

    os.remove("fast_urls.txt")

# Function to calculate the date range for the next 14 days
def calculate_date_range():
    today = datetime.now()
    start_date = today + timedelta(days=0)  # Start from tomorrow
    end_date = today + timedelta(days=25)   # End after 14 days
    return start_date.strftime('%Y-%m-%d 00:00:00'), end_date.strftime('%Y-%m-%d 00:00:00')

# Function to schedule the parse
def schedule_parse():
    start_date, end_date = calculate_date_range()
    print(f"Next parse will be done between {start_date} and {end_date}")
    
    schedule.every(25).days.do(run_parse)

    while True:
        time_left = schedule.idle_seconds()
        if time_left is not None:
            remaining_time = timedelta(seconds=time_left)
            print(f"Time left until next parse: {remaining_time}")
            time.sleep(21600)  # Wait for a minute before checking again
        schedule.run_pending()


if __name__ == "__main__":
    schedule_parse()